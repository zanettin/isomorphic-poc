# Angular 2 Universal Starter

A minimal Angular 2 starter for Universal JavaScript using TypeScript and Webpack

> If you're looking for the repo from the AngularConnect talk look in the [angular-connect branch](https://github.com/angular/universal-starter/tree/angular-connect)  
If you're looking for a SystemJS version of the repo look in the [systemjs branch](https://github.com/angular/universal-starter/tree/systemjs)

## Installation

* `npm install --global typescript typings webpack nodemon`
* `npm install`

## Serve

* `npm start` to build your client app and start a web server
* `npm run build` to prepare a distributable bundle

## Watch files
* `npm watch` to build your client app and start a web server

## SystemJS integration in this branch (with a little bit of gulp)
I've deleted the webpack build-job and integrated a simple "compileTS" as a new gulp-task to just compile everything in the app.
You're still able to just run "npm start", a new "prestart" target will execute the gulp compile task, and we can startup the server.ts directly by passing it trough "ts-node".
The tsconfig target is still "commonjs", because we compile both sides, client and server here with the same config and we can't load a systemjs module in express/node server-side.

## Auth0 Demo
There is a basic implementation in the App for auth0, check out:
* server.ts which contains new route /sessions/create
* additional classes in app.ts
* the all new Login component together with the directive "LoggedInOutlet"

Those are the most important parts, besides some new bootstrapping to get AuthHttp and AuthConfig running and new packages for jwt support.

This basic demo is not using real users from auth0 backend - it just implements a dummy mock Object with one user, demonstrating how we can auth for total custom users coming from our own db.

In the meanwhile i found a very good plunk which shows an ng-2 implementation of auth0 very isolated (and also cooler written as the official demo):
http://plnkr.co/edit/pKazgpkXLX9ZgV02YuVf?p=preview

## Remote Debugging with node-inspector
This is just pure awesome! Ever wanted to completely debug a node server app like our server.ts here?
`npm install -g node-inspector`

After the inspector is globally available, just manually startup the app by writing:

`node-debug src/server.js`

It will startup, and if everything went fine, he just opens up a new browser window, containing only one big DevTool instance -> pointing to your server-side code.

> node-inspector will only work in Chrome/Safari, if your default browser is different - manually open Chrome and enter the debug url which goes like "http://127.0.0.1:8080/?port=5858"
So it is also possible to remotely debug, just the ports need to be open.