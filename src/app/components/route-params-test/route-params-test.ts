import {Component} from 'angular2/core';
import {RouteParams} from 'angular2/router';

@Component({
  selector: 'RouteParamsTest',
  template: `
    <h1>Route Params Test</h1>
    <ul>
      <li>UserID (route param): <span class="badge">{{ userId }}</span></li>
      <li>Language (route param): <span class="badge">{{ language }}</span></li>
      <li>Token (query string): <span class="badge">{{ token }}</span></li>
    </ul>
  `
})
export class RouteParamsTest {

  userId    : string;
  language  : string;
  token     : string;

  constructor(private params : RouteParams) {

    this.userId   = params.get('userId');
    this.language = params.get('lang');
    this.token    = params.get('token');

  }

}
