"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var router_1 = require('angular2/router');
var RouteParamsTest = (function () {
    function RouteParamsTest(params) {
        this.params = params;
        this.userId = params.get('userId');
        this.language = params.get('lang');
        this.token = params.get('token');
    }
    RouteParamsTest = __decorate([
        core_1.Component({
            selector: 'RouteParamsTest',
            template: "\n    <h1>Route Params Test</h1>\n    <ul>\n      <li>UserID (route param): <span class=\"badge\">{{ userId }}</span></li>\n      <li>Language (route param): <span class=\"badge\">{{ language }}</span></li>\n      <li>Token (query string): <span class=\"badge\">{{ token }}</span></li>\n    </ul>\n  "
        }), 
        __metadata('design:paramtypes', [router_1.RouteParams])
    ], RouteParamsTest);
    return RouteParamsTest;
}());
exports.RouteParamsTest = RouteParamsTest;
//# sourceMappingURL=route-params-test.js.map