import {Component} from 'angular2/core';

@Component({
  selector: 'home',
  template: `
  <h1>Angular Unviversal PoC</h1>

  <div class="row">

    <div class="col-xs-3">
      <div class="thumbnail">
        <img src="https://angular.io/resources/images/logos/standard/shield-large.png" alt="" style="max-height: 180px">
        <div class="caption">
          <h4>angular2</h4>
          v. beta.11
        </div>
      </div>
    </div>

    <div class="col-xs-3">
      <div class="thumbnail">
        <img src="https://cloud.githubusercontent.com/assets/1016365/10639063/138338bc-7806-11e5-8057-d34c75f3cafc.png" alt="" style="max-height: 180px">
        <div class="caption">
          <h4>angular universal preview</h4>
          v. 0.82.1
        </div>
      </div>
    </div>

  </div>

  `
})
export class Home {
}
