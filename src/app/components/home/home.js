"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var Home = (function () {
    function Home() {
    }
    Home = __decorate([
        core_1.Component({
            selector: 'home',
            template: "\n  <h1>Angular Unviversal PoC</h1>\n\n  <div class=\"row\">\n\n    <div class=\"col-xs-3\">\n      <div class=\"thumbnail\">\n        <img src=\"https://angular.io/resources/images/logos/standard/shield-large.png\" alt=\"\" style=\"max-height: 180px\">\n        <div class=\"caption\">\n          <h4>angular2</h4>\n          v. beta.11\n        </div>\n      </div>\n    </div>\n\n    <div class=\"col-xs-3\">\n      <div class=\"thumbnail\">\n        <img src=\"https://cloud.githubusercontent.com/assets/1016365/10639063/138338bc-7806-11e5-8057-d34c75f3cafc.png\" alt=\"\" style=\"max-height: 180px\">\n        <div class=\"caption\">\n          <h4>angular universal preview</h4>\n          v. 0.82.1\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], Home);
    return Home;
}());
exports.Home = Home;
//# sourceMappingURL=home.js.map