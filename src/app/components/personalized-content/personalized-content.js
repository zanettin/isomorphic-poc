"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var router_1 = require('angular2/router');
var ng2_cookies_1 = require('ng2-cookies');
var PersonalizedContent = (function () {
    function PersonalizedContent(params) {
        this.params = params;
        this._update();
    }
    PersonalizedContent.prototype.login = function () {
        ng2_cookies_1.Cookie.setCookie('loginStatus', 'yes');
        this._update();
    };
    PersonalizedContent.prototype.logout = function () {
        ng2_cookies_1.Cookie.setCookie('loginStatus', 'no');
        this._update();
    };
    PersonalizedContent.prototype._update = function () {
        this.loginStatus = ng2_cookies_1.Cookie.getCookie('loginStatus');
    };
    PersonalizedContent = __decorate([
        core_1.Component({
            selector: 'PersonalizedContent',
            template: "\n    <h1>Personalized Content</h1>\n    logged in: <span class=\"badge\">{{ loginStatus }}</span>\n    <hr/>\n    <button class=\"btn btn-success\" (click)=\"login()\">flag as logged in</button>\n    <button class=\"btn btn-danger\" (click)=\"logout()\">flag as logged out</button>\n  "
        }), 
        __metadata('design:paramtypes', [router_1.RouteParams])
    ], PersonalizedContent);
    return PersonalizedContent;
}());
exports.PersonalizedContent = PersonalizedContent;
//# sourceMappingURL=personalized-content.js.map