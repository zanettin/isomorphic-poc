import {Component} from 'angular2/core';
import {RouteParams} from 'angular2/router';
import {Cookie} from 'ng2-cookies';

@Component({
  selector: 'PersonalizedContent',
  template: `
    <h1>Personalized Content</h1>
    logged in: <span class="badge">{{ loginStatus }}</span>
    <hr/>
    <button class="btn btn-success" (click)="login()">flag as logged in</button>
    <button class="btn btn-danger" (click)="logout()">flag as logged out</button>
  `
})
export class PersonalizedContent {

  loginStatus : any;

  constructor(private params : RouteParams) {

    this._update();

  }


  login() : void {

    Cookie.setCookie('loginStatus', 'yes');
    this._update();

  }

  logout() : void {

    Cookie.setCookie('loginStatus', 'no');
    this._update();

  }

  _update() : void {

    this.loginStatus = Cookie.getCookie('loginStatus');

  }

}
