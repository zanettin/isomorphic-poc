import {Component} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {RouteConfig, RouteParams, ROUTER_DIRECTIVES} from 'angular2/router';

import {XLarge} from '../../directives/xlarge.directive';

import {Home} from '../home/home';
import {About} from '../about/about';
import {ApiTest} from '../api-test/api-test';
import {RouteParamsTest} from '../route-params-test/route-params-test';
import {PersonalizedContent} from '../personalized-content/personalized-content';

@Component({
  selector: 'app',
  directives: [
    ...ROUTER_DIRECTIVES,
    XLarge
  ],
  styles: [`
    .router-link-active {
      background-color: lightgray;
    }
  `],
  template: `
  <div>

    <nav class="navbar navbar-default" style="padding: 0 20px;">
      <a class="navbar-btn btn btn-default" [routerLink]=" ['./Home'] ">Home</a>
      <a class="navbar-btn btn btn-default" [routerLink]=" ['./About'] ">About</a>
      <a class="navbar-btn btn btn-default" [routerLink]=" ['./ApiTest', { execute : 'button' }] ">API Test (indirect xhr)</a>
      <a class="navbar-btn btn btn-default" [routerLink]=" ['./ApiTest', { execute : 'onload' }] ">API Test (direct xhr)</a>
      <a class="navbar-btn btn btn-default" [routerLink]=" ['./RouteParamsTest', { userId : '1234', lang : 'de' }] ">Route Params Test 1</a>
      <a class="navbar-btn btn btn-default" [routerLink]=" ['./RouteParamsTest', { userId : '1919', lang : 'it', token : 'jwt' }] ">Route Params Test 2</a>
      <a class="navbar-btn btn btn-default" [routerLink]=" ['./PersonalizedContent'] ">Personalized Content</a>
    </nav>

    <div>
      <span x-large>Hello, {{ name }}!</span>
    </div>

    name: <input type="text" [value]="name" (input)="name = $event.target.value" autofocus>

    <hr/>

    <main>
      <router-outlet></router-outlet>
    </main>
  </div>
  `
})
@RouteConfig([
  { path: '/', component: Home, name: 'Home' },
  { path: '/home', component: Home, name: 'Home' },
  { path: '/about', component: About, name: 'About' },
  { path: '/api-test', component: ApiTest, name: 'ApiTest' },
  { path: '/route-params-test/:userId/:lang', component: RouteParamsTest, name: 'RouteParamsTest' },
  { path: '/personalized', component: PersonalizedContent, name: 'PersonalizedContent' }
])
export class App {
  name: string = 'Angular 2';
}


