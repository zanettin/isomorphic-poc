"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var router_1 = require('angular2/router');
var xlarge_directive_1 = require('../../directives/xlarge.directive');
var home_1 = require('../home/home');
var about_1 = require('../about/about');
var api_test_1 = require('../api-test/api-test');
var route_params_test_1 = require('../route-params-test/route-params-test');
var personalized_content_1 = require('../personalized-content/personalized-content');
var App = (function () {
    function App() {
        this.name = 'Angular 2';
    }
    App = __decorate([
        core_1.Component({
            selector: 'app',
            directives: router_1.ROUTER_DIRECTIVES.concat([
                xlarge_directive_1.XLarge
            ]),
            styles: ["\n    .router-link-active {\n      background-color: lightgray;\n    }\n  "],
            template: "\n  <div>\n\n    <nav class=\"navbar navbar-default\" style=\"padding: 0 20px;\">\n      <a class=\"navbar-btn btn btn-default\" [routerLink]=\" ['./Home'] \">Home</a>\n      <a class=\"navbar-btn btn btn-default\" [routerLink]=\" ['./About'] \">About</a>\n      <a class=\"navbar-btn btn btn-default\" [routerLink]=\" ['./ApiTest', { execute : 'button' }] \">API Test (indirect xhr)</a>\n      <a class=\"navbar-btn btn btn-default\" [routerLink]=\" ['./ApiTest', { execute : 'onload' }] \">API Test (direct xhr)</a>\n      <a class=\"navbar-btn btn btn-default\" [routerLink]=\" ['./RouteParamsTest', { userId : '1234', lang : 'de' }] \">Route Params Test 1</a>\n      <a class=\"navbar-btn btn btn-default\" [routerLink]=\" ['./RouteParamsTest', { userId : '1919', lang : 'it', token : 'jwt' }] \">Route Params Test 2</a>\n      <a class=\"navbar-btn btn btn-default\" [routerLink]=\" ['./PersonalizedContent'] \">Personalized Content</a>\n    </nav>\n\n    <div>\n      <span x-large>Hello, {{ name }}!</span>\n    </div>\n\n    name: <input type=\"text\" [value]=\"name\" (input)=\"name = $event.target.value\" autofocus>\n\n    <hr/>\n\n    <main>\n      <router-outlet></router-outlet>\n    </main>\n  </div>\n  "
        }),
        router_1.RouteConfig([
            { path: '/', component: home_1.Home, name: 'Home' },
            { path: '/home', component: home_1.Home, name: 'Home' },
            { path: '/about', component: about_1.About, name: 'About' },
            { path: '/api-test', component: api_test_1.ApiTest, name: 'ApiTest' },
            { path: '/route-params-test/:userId/:lang', component: route_params_test_1.RouteParamsTest, name: 'RouteParamsTest' },
            { path: '/personalized', component: personalized_content_1.PersonalizedContent, name: 'PersonalizedContent' }
        ]), 
        __metadata('design:paramtypes', [])
    ], App);
    return App;
}());
exports.App = App;
//# sourceMappingURL=app.js.map