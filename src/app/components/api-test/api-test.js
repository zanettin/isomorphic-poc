"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var http_1 = require('angular2/http');
var router_1 = require('angular2/router');
var ApiTest = (function () {
    function ApiTest(http, params) {
        this.http = http;
        this.params = params;
        this.loading = false;
        this.mode = params.get('execute');
        this.data = (this.mode === 'onload') ? 'loading' : 'click btn and wait...';
        if (this.mode === 'onload') {
            this.makeRequest();
        }
    }
    ApiTest.prototype.makeRequest = function () {
        var _this = this;
        this.loading = true;
        this.http
            .request('http://jsonplaceholder.typicode.com/posts')
            .subscribe(function (res) {
            _this.data = res.json();
            _this.loading = false;
        });
    };
    ApiTest = __decorate([
        core_1.Component({
            selector: 'ApiTest',
            template: "\n    <h1>api test</h1>\n    <button *ngIf=\"loading === false && mode !== 'onload'\" (click)=\"makeRequest()\" class=\"btn btn-primary\">get posts</button>\n    <hr/>\n    <pre>{{ data | json }}</pre>\n  "
        }), 
        __metadata('design:paramtypes', [http_1.Http, router_1.RouteParams])
    ], ApiTest);
    return ApiTest;
}());
exports.ApiTest = ApiTest;
//# sourceMappingURL=api-test.js.map