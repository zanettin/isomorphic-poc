import {Component} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {RouteParams} from 'angular2/router';

@Component({
  selector: 'ApiTest',
  template: `
    <h1>api test</h1>
    <button *ngIf="loading === false && mode !== 'onload'" (click)="makeRequest()" class="btn btn-primary">get posts</button>
    <hr/>
    <pre>{{ data | json }}</pre>
  `
})
export class ApiTest {

  data    : Object;
  loading : boolean;
  mode    : string;

  constructor(public http : Http, private params : RouteParams) {

    this.loading  = false;
    this.mode     = params.get('execute');
    this.data     = (this.mode === 'onload')  ? 'loading' : 'click btn and wait...';

    // fetch data directly from api endpoint
    if (this.mode === 'onload') {

      this.makeRequest();

    }

  }

  makeRequest() : void {

    this.loading = true;

    this.http
      .request('http://jsonplaceholder.typicode.com/posts')
      .subscribe(
      (res : Response) => {

        this.data = res.json();
        this.loading = false;

      }

    );

  }

}
