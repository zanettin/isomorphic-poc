/// <reference path="../typings/browser.d.ts" />

// import {bootstrap} from 'angular2/platform/browser';
import {bootstrap} from 'angular2-universal-preview';
import {Router, ROUTER_PROVIDERS} from 'angular2/router';
import { HTTP_PROVIDERS } from 'angular2/http';


import {App} from './app/components/app/app';

bootstrap(App, [
  HTTP_PROVIDERS,
  ...ROUTER_PROVIDERS
]);
