"use strict";
var angular2_universal_preview_1 = require('angular2-universal-preview');
var router_1 = require('angular2/router');
var http_1 = require('angular2/http');
var app_1 = require('./app/components/app/app');
angular2_universal_preview_1.bootstrap(app_1.App, [
    http_1.HTTP_PROVIDERS
].concat(router_1.ROUTER_PROVIDERS));
//# sourceMappingURL=client.js.map