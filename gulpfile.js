var gulp = require('gulp');
var ts = require('gulp-typescript');
//var rimraf = require('gulp-rimraf');

var tsProject = ts.createProject('tsconfig.json');

gulp.task('compileTS', function () {
  var tsResult = gulp.src('src/**/*.ts')
    .pipe(ts(tsProject));

  return tsResult.js.pipe(gulp.dest('src'));
});

gulp.task('default', ['compileTS'], function () {

});
